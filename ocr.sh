#!/bin/bash
VERSION=0.2

IMG_SRC="/home/$USER/Pictures/screenshots"
LANG="eng"

check_req() {
        if [ -z "$(which tesseract)" ]; then
		echo "tesseract not installed, quitting."
		exit 0
        fi
}

print_help(){
	echo "OCR ver.: " $VERSION
	echo ""
	echo "if no args provide, gets the newest file from $IMG_SRC"
	echo "-l <lang>		- tesseract language code"
	echo "-f <filepath>		- path to file"
	echo "-h	 		- shows this menu"
	exit

}

create_dirs(){
	if [ ! -d $IMG_SRC ]; then
		mkdir -p $IMG_SRC
	fi
}

lang_set() {
    if [ -n "$1" ]; then
        LANG="$1"
    fi
    echo "Set language: $LANG"
}

filename() {
	FILE="$1"
}

main() {

	if [ -z "$FILE" ]; then
		IMG=$(ls -t "$IMG_SRC" -I "*.txt*" | head -n1)
		tesseract -l "$LANG" "$IMG_SRC/$IMG" "$IMG_SRC/$IMG"
		echo "Processing: " $IMG_SRC/$IMG
		echo "Output: " $IMG_SRC/$IMG.txt
		xclip -sel c < "$IMG_SRC/$IMG.txt"

	else
		tesseract -l "$LANG" "$FILE" "$FILE"
		echo "Processing: " $FILE
		echo "Output: " $FILE.txt
		xclip -sel c < "$FILE.txt"

	fi
}

while getopts ":l:f:h" opt; do
    case ${opt} in
        l )
		lang_set "$OPTARG"
		;;
	f )
		filename "$OPTARG"
		;;
	h )
		print_help
		;;
        \? )
		echo "Invalid option: -$OPTARG" >&2
		exit 1
		;;
        : )
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;
    esac
done

check_req
main
